package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellLeaveReportBinding

class LeaveReportAdapter : RecyclerView.Adapter<LeaveReportAdapter.LeaveReportViewHolder>() {
    inner class LeaveReportViewHolder(binding: CellLeaveReportBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaveReportViewHolder {
        val view =
            CellLeaveReportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LeaveReportViewHolder(view)

    }

    override fun onBindViewHolder(holder: LeaveReportViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}