package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellPaySlipBinding

class PaySlipAdapter : RecyclerView.Adapter<PaySlipAdapter.PaySlipViewHolder>() {

    inner class PaySlipViewHolder(itemBinding: CellPaySlipBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaySlipViewHolder {
        val view = CellPaySlipBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PaySlipViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaySlipViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}