package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellFormsBinding

class FormsAdapter:RecyclerView.Adapter<FormsAdapter.FormsViewHolder>() {

    inner class FormsViewHolder(itemBinding:CellFormsBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FormsViewHolder {

        val view=CellFormsBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return FormsViewHolder(view)
    }

    override fun onBindViewHolder(holder: FormsViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5

    }
}