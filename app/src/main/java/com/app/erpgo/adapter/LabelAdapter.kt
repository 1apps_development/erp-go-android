package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellLabelBinding

class LabelAdapter():RecyclerView.Adapter<LabelAdapter.LabelViewHolder>() {
    inner class LabelViewHolder(itemBinding:CellLabelBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LabelViewHolder {
        val view=CellLabelBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return LabelViewHolder(view)

    }

    override fun onBindViewHolder(holder: LabelViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}