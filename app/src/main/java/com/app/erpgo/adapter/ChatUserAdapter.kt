package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellChatUsersBinding
import com.app.erpgo.databinding.CellUsersBinding

class ChatUserAdapter:RecyclerView.Adapter<ChatUserAdapter.ChatUserViewHolder>() {

    inner class ChatUserViewHolder(binding:CellChatUsersBinding):RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatUserViewHolder {

        val view=CellChatUsersBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ChatUserViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatUserViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5

    }
}