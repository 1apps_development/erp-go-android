package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellMeetingBinding

class MeetingListAdapter :RecyclerView.Adapter<MeetingListAdapter.MeetingListViewHolder>() {
    inner class MeetingListViewHolder(binding:CellMeetingBinding):RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeetingListViewHolder {
        val view=CellMeetingBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MeetingListViewHolder(view)

    }

    override fun onBindViewHolder(holder: MeetingListViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}