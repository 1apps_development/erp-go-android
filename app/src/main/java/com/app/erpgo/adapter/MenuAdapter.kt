package com.app.erpgo.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.R
import com.app.erpgo.databinding.CellMenuBinding
import com.app.erpgo.model.MenuItemData

import com.bumptech.glide.Glide


class MenuAdapter(private var context: Context,var menuItemList: ArrayList<MenuItemData>,private val listener: (String, Int) -> Unit):RecyclerView.Adapter<MenuAdapter.MenuViewHolder>() {


    inner class MenuViewHolder(var itemBinding: CellMenuBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bindItem(data: MenuItemData, context: Context,listener: (String, Int) -> Unit,position: Int) {
            itemBinding.apply {
                Glide.with(context).load(data.itemIcon).placeholder(R.drawable.ic_place_holder).into(ivMenu)
                tvMenuName.text = data.itemName

                if(menuItemList[position].isSelect)
                {
                    menuCard.setCardBackgroundColor(ColorStateList.valueOf( ResourcesCompat.getColor(context.resources,R.color.lime_green,null)))
                    tvMenuName.setTextColor(ColorStateList.valueOf(ResourcesCompat.getColor(context.resources, R.color.white, null)))
                    ivMenu.imageTintList = ColorStateList.valueOf(Color.WHITE)

                }else
                {
                    menuCard.setCardBackgroundColor(ColorStateList.valueOf( ResourcesCompat.getColor(context.resources,R.color.white,null)))
                    tvMenuName.setTextColor(ColorStateList.valueOf(ResourcesCompat.getColor(context.resources, R.color.colorPrimary, null)))
                    ivMenu.imageTintList = ColorStateList.valueOf(ResourcesCompat.getColor(context.resources, R.color.colorPrimary, null))

                }

                itemView.setOnClickListener {
                listener("itemClick",adapterPosition)
                notifyDataSetChanged()
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuViewHolder {
        val view = CellMenuBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MenuViewHolder(view)

    }

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) {
        holder.bindItem(menuItemList[position], context,listener,position)
    }

    override fun getItemCount(): Int {
        return menuItemList.size
    }
}