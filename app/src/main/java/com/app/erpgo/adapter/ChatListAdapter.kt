package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellChatBinding
import com.app.erpgo.util.Constants

class ChatListAdapter (var listener:(Int,String)->Unit): RecyclerView.Adapter<ChatListAdapter.ChatListViewHolder>() {

    inner class ChatListViewHolder(binding: CellChatBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListViewHolder {
        val view = CellChatBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChatListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatListViewHolder, position: Int) {

        holder.itemView.setOnClickListener {

            listener(position,Constants.ItemClick)
        }
    }

    override fun getItemCount(): Int {
        return 5
    }
}