package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellLatestIncomeItemBinding
import com.app.erpgo.databinding.CellSupportBinding

class SupportListAdapter : RecyclerView.Adapter<SupportListAdapter.SupportListViewHolder>() {

    inner class SupportListViewHolder(itemBinding: CellSupportBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupportListViewHolder {
        val view = CellSupportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SupportListViewHolder(view)
    }

    override fun onBindViewHolder(holder: SupportListViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }
}