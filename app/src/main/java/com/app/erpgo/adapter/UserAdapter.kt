package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellUsersBinding

class UserAdapter :RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
    inner class UserViewHolder(itemBinding:CellUsersBinding) :RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view=CellUsersBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 4
    }
}