package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellTodoListBinding

class TodoListAdapter:RecyclerView.Adapter<TodoListAdapter.TodoListViewHolder>(){

    inner class TodoListViewHolder(itemBinding:CellTodoListBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoListViewHolder {
        val view=CellTodoListBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return TodoListViewHolder(view)
    }

    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}