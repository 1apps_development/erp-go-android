package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellLatestIncomeItemBinding

class LatestIncomeAdapter() : RecyclerView.Adapter<LatestIncomeAdapter.LatestIncomeViewHolder>() {
    inner class LatestIncomeViewHolder(binding: CellLatestIncomeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LatestIncomeViewHolder {
        val view =
            CellLatestIncomeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LatestIncomeViewHolder(view)
    }

    override fun onBindViewHolder(holder: LatestIncomeViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}