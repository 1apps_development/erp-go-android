package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellLatestIncomeItemBinding
import com.app.erpgo.databinding.CellMembersBinding
import kotlin.time.measureTime

class MemberListAdapter:RecyclerView.Adapter<MemberListAdapter.MemberViewHolder>()  {
    inner class MemberViewHolder(itemBinding: CellMembersBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemberViewHolder {
        val view=CellMembersBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MemberViewHolder(view)
    }

    override fun onBindViewHolder(holder: MemberViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}