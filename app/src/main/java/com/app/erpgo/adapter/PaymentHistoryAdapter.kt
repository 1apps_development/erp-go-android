package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellPaymentHistoryBinding

class PaymentHistoryAdapter :RecyclerView.Adapter<PaymentHistoryAdapter.PaymentHistoryViewHolder>() {
inner class PaymentHistoryViewHolder(binding:CellPaymentHistoryBinding):RecyclerView.ViewHolder(binding.root)
{

}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHistoryViewHolder {
        val view=CellPaymentHistoryBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return PaymentHistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentHistoryViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }

}