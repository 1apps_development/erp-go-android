package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellJoblistBinding

class JobListAdapter:RecyclerView.Adapter<JobListAdapter.JobListViewHolder>() {
    inner class JobListViewHolder(itemBinding: CellJoblistBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JobListViewHolder {
        val view=CellJoblistBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return JobListViewHolder(view)
    }

    override fun onBindViewHolder(holder: JobListViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
            return 5
    }


}