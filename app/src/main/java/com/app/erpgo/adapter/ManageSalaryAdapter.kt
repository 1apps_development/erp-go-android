package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellEmployeeSalaryBinding

class ManageSalaryAdapter :RecyclerView.Adapter<ManageSalaryAdapter.ManageSalaryViewHolder>() {


    inner class ManageSalaryViewHolder(binding:CellEmployeeSalaryBinding):RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManageSalaryViewHolder {
        val view=CellEmployeeSalaryBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ManageSalaryViewHolder(view)
    }

    override fun onBindViewHolder(holder: ManageSalaryViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}