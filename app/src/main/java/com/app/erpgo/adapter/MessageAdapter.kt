package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellReceiverBinding
import com.app.erpgo.databinding.CellSenderBinding
import com.app.erpgo.model.Message

class MessageAdapter(var messageList:ArrayList<Message>) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var senderType=0
    private var receiverType=1

    inner class SenderViewHolder(binding:CellSenderBinding):RecyclerView.ViewHolder(binding.root)
    {

    }
    inner class ReceiverViewHolder(binding:CellReceiverBinding):RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if(viewType==senderType) {
            val view=CellSenderBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            SenderViewHolder(view)
        } else {
            val view=CellReceiverBinding.inflate(LayoutInflater.from(parent.context),parent,false)
            ReceiverViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


    }

    override fun getItemViewType(position: Int): Int {

        return if(messageList[position].type==1) {
            senderType
        }else {
            receiverType
        }

    }

    override fun getItemCount(): Int {

        return messageList.size
    }
}