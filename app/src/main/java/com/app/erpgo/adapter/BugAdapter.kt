package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellBugBinding

class BugAdapter : RecyclerView.Adapter<BugAdapter.BugViewHolder>() {

    inner class BugViewHolder(itemBinding: CellBugBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BugViewHolder {
        val view = CellBugBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BugViewHolder(view)
    }

    override fun onBindViewHolder(holder: BugViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}