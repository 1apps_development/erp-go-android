package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellClientsBinding
import com.app.erpgo.databinding.CellLatestIncomeItemBinding
import com.app.erpgo.util.Constants

class ClientsAdapter(var listener:(Int,String)->Unit) : RecyclerView.Adapter<ClientsAdapter.ClientsViewHolder>() {
    inner class ClientsViewHolder(itemBinding: CellClientsBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientsViewHolder {

        val view = CellClientsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ClientsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClientsViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            listener(position,Constants.ItemClick)
        }
    }

    override fun getItemCount(): Int {
        return 5

    }
}