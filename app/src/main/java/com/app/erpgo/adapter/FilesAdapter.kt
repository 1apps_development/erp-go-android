package com.app.erpgo.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.databinding.CellAttachmentsBinding

class FilesAdapter : RecyclerView.Adapter<FilesAdapter.FilesViewHolder>() {
    inner class FilesViewHolder(itemBinding: CellAttachmentsBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilesViewHolder {
        val view =
            CellAttachmentsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FilesViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilesViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}