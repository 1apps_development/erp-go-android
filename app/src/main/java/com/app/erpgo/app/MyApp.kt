package com.app.erpgo.app

import android.app.Application
import androidx.multidex.MultiDex

class MyApp :Application() {

    companion object {
        lateinit var app: MyApp



        fun getInstance(): MyApp {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this
     /*      ViewPump.init(
               ViewPump.builder()
                   .addInterceptor(
                       CalligraphyInterceptor(
                           CalligraphyConfig.Builder()
                               .setDefaultFontPath("font/poppins_regular.ttf")
                               .setFontAttrId(R.attr.fontPath)
                               .build()
                       )
                   )
                   .build()
           )*/
        MultiDex.install(this)

    }
}