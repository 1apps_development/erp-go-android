package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.R
import com.app.erpgo.databinding.ActCompanySettingsBinding

class ActCompanySettings : AppCompatActivity() {
    private lateinit var binding:ActCompanySettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActCompanySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}