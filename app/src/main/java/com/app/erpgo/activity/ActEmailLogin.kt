package com.app.erpgo.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.erpgo.databinding.ActEmailLoginBinding


class ActEmailLogin : AppCompatActivity() {

    private lateinit var binding: ActEmailLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActEmailLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClickListeners()
    }


    private fun initClickListeners() {
        binding.btnLogin.setOnClickListener {
            startActivity(Intent(this@ActEmailLogin, MainActivity::class.java))

        }
        binding.tvForgotPassword.setOnClickListener {

            startActivity(Intent(this@ActEmailLogin, ActForgotPassword::class.java))
        }

    }
}