package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.R
import com.app.erpgo.databinding.ActJobDetailsBinding

class ActJobDetails : AppCompatActivity() {
    private lateinit var binding:ActJobDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActJobDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}