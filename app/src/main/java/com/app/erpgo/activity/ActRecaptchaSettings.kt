package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.R
import com.app.erpgo.databinding.ActRecaptchaSettingsBinding

class ActRecaptchaSettings : AppCompatActivity() {
    private lateinit var binding:ActRecaptchaSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActRecaptchaSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}