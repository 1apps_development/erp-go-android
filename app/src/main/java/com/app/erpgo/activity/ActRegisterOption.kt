package com.app.erpgo.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.databinding.ActRegisterOptionBinding

class ActRegisterOption : AppCompatActivity() {
    private lateinit var binding: ActRegisterOptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActRegisterOptionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
    }


    private fun initClickListeners()
    {
        binding.linearEmail.setOnClickListener {
            startActivity(Intent(this@ActRegisterOption,ActSignup::class.java))
        }
    }
}