package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.PaymentHistoryAdapter
import com.app.erpgo.databinding.ActClientDetailBinding

class ActClientDetail : AppCompatActivity() {
    private lateinit var binding:ActClientDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActClientDetailBinding.inflate(layoutInflater)
        setupAdapter()
        setContentView(binding.root)
    }

    private fun setupAdapter()
    {
        binding.rvPaymentHistory.apply {

            layoutManager=LinearLayoutManager(this@ActClientDetail)
            adapter=PaymentHistoryAdapter()

        }
    }
}