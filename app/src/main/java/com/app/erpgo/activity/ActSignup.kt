package com.app.erpgo.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.databinding.ActSignupBinding


class ActSignup : AppCompatActivity() {
    private lateinit var binding: ActSignupBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActSignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSignUp.setOnClickListener {
            startActivity(Intent(this@ActSignup, MainActivity::class.java))
        }
    }
}