package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.MenuAdapter
import com.app.erpgo.databinding.ActivityMainBinding
import com.app.erpgo.databinding.DialogMenuBinding
import com.app.erpgo.fragments.*
import com.app.erpgo.model.MenuItemData
import com.app.erpgo.util.Common
import com.github.techisfun.android.topsheet.TopSheetDialog

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    private var itemList = ArrayList<MenuItemData>()
    private lateinit var menuAdapter: MenuAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bottomNavigation.selectedItemId = R.id.ivDashBoard

        menuItemData()
        initClickListeners()
        Common.replaceFragment(
            supportFragmentManager,
            FragDashBoard(),
            R.id.fragContainer
        )

        bottomBarItemNavigation()
    }

    private fun menuSelection(pos: Int) {
        for (j in 0 until itemList.size) {
            itemList[j].isSelect = false

        }
        itemList[pos].isSelect = true
    }


    private fun initClickListeners()
    {
        binding.ivDrawer.setOnClickListener {
            openTopSheetDialog()
        }
    }
    private fun openTopSheetDialog() {
        val dialog = TopSheetDialog(this)
        val bottomSheetBinding = DialogMenuBinding.inflate(layoutInflater)

        menuAdapter = MenuAdapter(this@MainActivity, itemList) { s: String, i: Int ->
            if (s == "itemClick") {
                val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
                dialog.dismiss()

                if (i == 0) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
                    if (fragment !is FragDashBoard) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragDashBoard(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 1) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.staff)
                    if (fragment !is FragClients) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragClients(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 2) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.product_searvices)
                    if (fragment !is FragProductService) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragProductService(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 3) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.crm)
                    if (fragment !is FragCrm) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragCrm(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 4) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.projects)
                    if (fragment !is FragProjects) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragProjects(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 5) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.hrm)
                    if (fragment !is FragPlans) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragPlans(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 6) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)
                    if (fragment !is FragSettings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSettings(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 7) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.zoom_meeting)
                    if (fragment !is FragMeeting) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragMeeting(),
                            R.id.fragContainer
                        )
                    }
                }

                else if (i == 8) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.support_list)
                    if (fragment !is FragSupport) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSupport(),
                            R.id.fragContainer
                        )
                    }
                }else if (i == 9) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.messages)
                    if (fragment !is FragChat) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragChat(),
                            R.id.fragContainer
                        )
                    }
                }


                else if (i == 10) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.plans)
                    if (fragment !is FragPlans) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragPlans(),
                            R.id.fragContainer
                        )
                    }
                }
                else if (i == 11) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.plans)
                    if (fragment !is FragTimeSheet) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTimeSheet(),
                            R.id.fragContainer
                        )
                    }
                }
                else if (i == 12) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.leave_report)
                    if (fragment !is FragLeaveReport) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragLeaveReport(),
                            R.id.fragContainer
                        )
                    }
                }

                else if (i == 13) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)
                    if (fragment !is FragSettings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSettings(),
                            R.id.fragContainer
                        )
                    }
                }

                for (j in 0 until itemList.size) {
                    itemList[j].isSelect = false

                }
                itemList[i].isSelect = true
                menuAdapter.notifyDataSetChanged()
            }

        }
        bottomSheetBinding.ivClose.setOnClickListener {
            dialog.dismiss()
        }
        bottomSheetBinding.rvMenu.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            adapter = menuAdapter
            isNestedScrollingEnabled = true
        }
        dialog.setContentView(bottomSheetBinding.root)
        dialog.show()
    }


    private fun bottomBarItemNavigation() {
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
            when (item.itemId) {
                R.id.ivStaff -> {
                    binding.tvAppBarTitle.text = resources.getString(R.string.clients)
                    menuSelection(1)
                    if (fragment !is FragClients) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragClients(),
                            R.id.fragContainer
                        )
                    }

                    true


                }
                R.id.ivProducts -> {
                    menuSelection(2)

                    binding.tvAppBarTitle.text = resources.getString(R.string.products)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragProducts(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivDashBoard -> {
                    menuSelection(0)

                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragDashBoard(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivCrm -> {
                    menuSelection(3)

                    binding.tvAppBarTitle.text = resources.getString(R.string.forms_list)

                    Common.replaceFragment(supportFragmentManager, FragFormList(), R.id.fragContainer)

                    true
                }

                R.id.ivProjects -> {
                    menuSelection(4)

                    binding.tvAppBarTitle.text = resources.getString(R.string.projects)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragProjects(),
                        R.id.fragContainer
                    )

                    true
                }
                else -> {
                    false
                }
            }
        }
    }


    private  fun menuItemData()
    {
        itemList.add(MenuItemData(resources.getString(R.string.dashboard),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_dashboard,null)!!,true))
        itemList.add(MenuItemData(resources.getString(R.string.staff),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_staff,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.product_searvices),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_cart,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.crm),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_crm,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.projects),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_projects,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.hrm),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_user,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.account),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_categories,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.zoom),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_zoom,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.support),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_support,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.messenger),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_messenger,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.plans),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_plans,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.order),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_bag,null)!!,false))

        itemList.add(MenuItemData(resources.getString(R.string.landing_page),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_monitor,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.settings),
            ResourcesCompat.getDrawable(resources,R.drawable.ic_settings,null)!!,false))
    }


    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        }else
        {
            supportFragmentManager.popBackStack()
            super.onBackPressed()
        }
    }


    fun callZoomMeeting()
    {
        binding.tvAppBarTitle.text = resources.getString(R.string.zoom_meeting)

        Common.replaceFragment(
            supportFragmentManager,
            FragMeeting(),
            R.id.fragContainer
        )
    }
    fun setAppBarTitle(title:String)
    {
        binding.tvAppBarTitle.text=title
    }
}