package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.databinding.ActPaymentDetailsBinding
import com.app.erpgo.util.ExtensionFunctions.gone
import com.app.erpgo.util.ExtensionFunctions.show


class ActPaymentDetails : AppCompatActivity() {
    private lateinit var binding: ActPaymentDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActPaymentDetailsBinding.inflate(layoutInflater)

        binding.ivBack.setOnClickListener {
            finish()
        }



        setContentView(binding.root)
        binding.swSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (!compoundButton.isChecked) {

                binding.tvStripeKeyTitle.gone()
                binding.edStripeKey.gone()
                binding.tvStripeSecret.gone()
                binding.edStripeSecret.gone()
            } else {
                binding.tvStripeKeyTitle.show()
                binding.edStripeKey.show()
                binding.tvStripeSecret.show()
                binding.edStripeSecret.show()
            }

        }
    }


}