package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.MessageAdapter
import com.app.erpgo.databinding.ActMessageBinding
import com.app.erpgo.model.Message

class ActMessage : AppCompatActivity() {
    private lateinit var binding: ActMessageBinding
    private var messageList = ArrayList<Message>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(2, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(2, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        messageList.add(Message(1, "Lorem Ipsum is simply dummy text "))
        setupAdapter()
    }

    private fun setupAdapter() {
        binding.rvMessages.apply {
            layoutManager=LinearLayoutManager(this@ActMessage)
            adapter=MessageAdapter(messageList)
        }
        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}