package com.app.erpgo.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.erpgo.R
import com.app.erpgo.databinding.ActSystemSettingsBinding

class ActSystemSettings : AppCompatActivity() {
    private lateinit var binding:ActSystemSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActSystemSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}