package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.adapter.TimeSheetAdapter
import com.app.erpgo.databinding.FragTimeSheetBinding



class FragTimeSheet : Fragment() {
    private lateinit var binding: FragTimeSheetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragTimeSheetBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }


    private fun setupAdapter() {
        binding.rvTimeList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = TimeSheetAdapter()
            isNestedScrollingEnabled = true
        }
    }

}