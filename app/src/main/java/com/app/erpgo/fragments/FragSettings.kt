package com.app.erpgo.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.erpgo.R
import com.app.erpgo.activity.*
import com.app.erpgo.databinding.FragSettingsBinding

class FragSettings : Fragment() {
    private lateinit var binding:FragSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragSettingsBinding.inflate(layoutInflater)
        initClickListeners()
        return binding.root
    }
    private fun initClickListeners()
    {
        binding.constraintPaymentSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActPaymentDetails::class.java))
        }
        binding.constraintCompanySettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActCompanySettings::class.java))

        }
        binding.constraintBusinessSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActCompanySettings::class.java))

        }

        binding.constraintSystemSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActSystemSettings::class.java))

        }
        binding.constraintPaymentSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActPaymentDetails::class.java))

        }
        binding.constraintSlackSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActSlackSettings::class.java))

        }
        binding.constraintReCaptchaSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActRecaptchaSettings::class.java))

        }
        binding.constraintZoomSettings.setOnClickListener {
            (requireActivity() as MainActivity).callZoomMeeting()
        }
    }
}