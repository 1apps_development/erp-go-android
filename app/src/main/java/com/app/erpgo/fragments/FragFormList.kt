package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.FormsAdapter
import com.app.erpgo.databinding.FragFormListBinding

class FragFormList : Fragment() {

    private lateinit var binding:FragFormListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragFormListBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }

    private fun setupAdapter()
    {
        binding.rvFormList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=FormsAdapter()

        }
    }


}