package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.erpgo.databinding.FragCrmBinding

class FragCrm : Fragment() {
  private lateinit var binding:FragCrmBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
            binding= FragCrmBinding.inflate(layoutInflater)
        return binding.root
    }


}