package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.adapter.LatestIncomeAdapter
import com.app.erpgo.adapter.ProductListAdapter
import com.app.erpgo.databinding.FragDashboardBinding


class FragDashBoard : Fragment() {
    private lateinit var binding :FragDashboardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragDashboardBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }

    private fun setupAdapter()
    {
        binding.rvProductList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=ProductListAdapter()
            addItemDecoration(DividerItemDecoration(requireActivity(),LinearLayout.VERTICAL))
        }

        binding.rvLatestIncome.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=LatestIncomeAdapter()
            addItemDecoration(DividerItemDecoration(requireActivity(),LinearLayout.VERTICAL))
        }
    }


}