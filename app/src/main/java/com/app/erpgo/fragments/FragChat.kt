package com.app.erpgo.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.erpgo.activity.ActMessage
import com.app.erpgo.adapter.ChatListAdapter
import com.app.erpgo.adapter.ChatUserAdapter
import com.app.erpgo.databinding.FragChatBinding
import com.app.erpgo.util.Constants


class FragChat : Fragment() {
    private lateinit var binding:FragChatBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragChatBinding.inflate(layoutInflater)
        setupAdapter()
        return  binding.root
    }

    private fun setupAdapter()
    {

        binding.rvUsers.apply {
            layoutManager=LinearLayoutManager(requireActivity(),RecyclerView.HORIZONTAL,false)
            adapter=ChatUserAdapter()
        }
        binding.rvChats.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=ChatListAdapter{pos:Int,type:String ->
                if(type==Constants.ItemClick)
                {
                    val intent=Intent(requireActivity(),ActMessage::class.java)
                    startActivity(intent)
                }
            }
        }

        binding.constraintNewMessage.setOnClickListener {

            startActivity(Intent(requireActivity(),ActMessage::class.java))
        }
    }


}