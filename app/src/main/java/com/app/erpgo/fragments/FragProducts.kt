package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.adapter.FilesAdapter
import com.app.erpgo.adapter.MemberListAdapter
import com.app.erpgo.adapter.ProductsAdapter
import com.app.erpgo.databinding.FragProductsBinding


class FragProducts : Fragment() {
    private lateinit var binding:FragProductsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragProductsBinding.inflate(layoutInflater)
        setupFilesAdapter()
        return binding.root
    }


    private fun setupFilesAdapter()
    {
        binding.rvMemberList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=MemberListAdapter()
        }
        binding.rvAllProduct.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=ProductsAdapter()
        }

        binding.rvAttachmentsList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=FilesAdapter()
        }
    }


}