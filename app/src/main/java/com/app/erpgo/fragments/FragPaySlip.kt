package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.PaySlipAdapter
import com.app.erpgo.databinding.FragPaySlipBinding


class FragPaySlip : Fragment() {
  private lateinit var binding:FragPaySlipBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragPaySlipBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }
    private fun setupAdapter()
    {
        binding.rvPaySlip.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=PaySlipAdapter()
        }
    }


}