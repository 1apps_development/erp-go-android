package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.adapter.TodoListAdapter
import com.app.erpgo.databinding.FragProjectsBinding

class FragProjects : Fragment() {
    private lateinit var binding:FragProjectsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragProjectsBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }

    private fun setupAdapter()
    {
        binding.rvTodoList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=TodoListAdapter()
        }
    }


}