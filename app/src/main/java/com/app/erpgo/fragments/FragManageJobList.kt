package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.JobListAdapter
import com.app.erpgo.databinding.FragManageJobListBinding

class FragManageJobList : Fragment() {
    private lateinit var binding: FragManageJobListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragManageJobListBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }

    private fun setupAdapter() {
        binding.rvJobList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = JobListAdapter()
        }
    }


}