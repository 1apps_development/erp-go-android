package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.SupportListAdapter
import com.app.erpgo.databinding.FragSupportBinding

class FragSupport : Fragment() {

    private lateinit var binding: FragSupportBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragSupportBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }


    private fun setupAdapter() {
        binding.rvSupportList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = SupportListAdapter()
        }
    }


}