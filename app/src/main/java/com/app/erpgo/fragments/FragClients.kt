package com.app.erpgo.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.activity.ActClientDetail
import com.app.erpgo.adapter.ClientsAdapter
import com.app.erpgo.adapter.UserAdapter
import com.app.erpgo.databinding.FragClientsBinding
import com.app.erpgo.databinding.NewClientDialogBinding
import com.app.erpgo.util.Constants
import com.google.android.material.bottomsheet.BottomSheetDialog


class FragClients : Fragment() {

    private lateinit var binding:FragClientsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragClientsBinding.inflate(layoutInflater)
        setupAdapter()
        initClickListeners()
        return binding.root
    }
    private fun setupAdapter()
    {
        binding.rvClients.apply {
            layoutManager= LinearLayoutManager(requireActivity())
            adapter= ClientsAdapter{pos:Int,type:String->
                run {
                    if (type == Constants.ItemClick) {
                        startActivity(Intent(requireActivity(), ActClientDetail::class.java))
                    }
                }
            }
        }
    }
    private fun initClickListeners()
    {
        binding.btnAddNewClient.setOnClickListener {
            newClientsDialog()
        }
    }
    private fun newClientsDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        val bottomSheetBinding = NewClientDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.show()
    }

}