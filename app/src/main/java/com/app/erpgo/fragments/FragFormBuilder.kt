package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.erpgo.R
import com.app.erpgo.databinding.FragFormBuilderBinding

class FragFormBuilder : Fragment() {
private lateinit var binding:FragFormBuilderBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragFormBuilderBinding.inflate(layoutInflater)
        return binding.root
    }


}