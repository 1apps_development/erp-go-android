package com.app.erpgo.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.erpgo.R
import com.app.erpgo.adapter.LeaveReportAdapter
import com.app.erpgo.databinding.FragLeaveReportBinding


class FragLeaveReport : Fragment() {
    private lateinit var binding: FragLeaveReportBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragLeaveReportBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }

    private fun setupAdapter() {
        binding.rvLeaveReport.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = LeaveReportAdapter()
        }
    }


}