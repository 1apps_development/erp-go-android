package com.app.erpgo.model

import android.graphics.drawable.Drawable

data class MenuItemData (var itemName:String,var  itemIcon:Drawable,var isSelect :Boolean=false)